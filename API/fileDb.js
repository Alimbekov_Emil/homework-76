const fs = require("fs");
const { nanoid } = require("nanoid");

const filename = "./db.json";
let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getMessages() {
    if (data.length < 30) {
      return data;
    } else {
      return data.slice(data.length - 30);
    }
  },
  addMessage(message) {
    message.id = nanoid();
    message.datetime = new Date().toISOString();
    data.push(message);
    this.save();
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data, null, 2));
  },
  getMessagesTimes(datetime) {
    return data.filter((message) => {
      return message.datetime > datetime;
    });
  },
};
