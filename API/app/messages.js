const express = require("express");
const fileDb = require("../fileDb");
const router = express.Router();

router.get("/", (req, res) => {
  if (req.query.datetime) {
    const date = new Date(req.query.datetime);
    if (date.toString() === "Invalid Date") {
      return res.status(400).send("You entered the wrong date");
    }
    const messagesDate = fileDb.getMessagesTimes(req.query.datetime);
    return res.send(messagesDate);
  }

  const messages = fileDb.getMessages();
  return res.send(messages);
});

router.post("/", (req, res) => {
  if (req.body.author === "" || req.body.message === "") {
    return res.status(400).send({ error: "Author and message must be present in the request" });
  }
  fileDb.addMessage(req.body);
  return res.send(req.body);
});

module.exports = router;
